import styled from 'styled-components'
import { FormControl } from '@material-ui/core'

export const Content = styled.div`
  flex-grow: 1;
  min-height: 100vh;
  ${(props) => props.theme.breakpoints.up('md')} {
    flex-grow: initial;
  }
  background-color: ${(props) => props.theme.palette.colors.pureBlack};
  padding: 20px;
`

export const Form = styled(FormControl)`
  margin-bottom: 20px;
`

export const Title = styled.h3`
  ${(props) => ({ ...props.theme.typography.h1 })}
  color: ${(props) => props.theme.palette.colors.pureWhite};
  text-align: center;
  margin-top: 0;
  margin-bottom: 20px;
`

export const ModalContent = styled.div`
  width: 100%;
  max-width: 600px;
  margin: 200px auto;
`
