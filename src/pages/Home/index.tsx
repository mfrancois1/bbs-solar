import React, { useEffect, useMemo, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import HomeTemplate, { HomeTemplateProps } from '../../templates/Home'
import { Planet } from '../../components/SolarProvider/types'
import { useSolarContext } from '../../components/SolarProvider'

const DashboardPage: React.FC<RouteComponentProps<{ planetId: string }>> = (props) => {
  const { t } = useTranslation()
  const { getById } = useSolarContext()
  const planetId = useMemo(() => props.match.params.planetId, [props.match.params.planetId])
  const [current, setCurrent] = useState<Planet | null>(null)

  useEffect(() => {
    if (planetId) {
      getById(planetId)
        .then((p) => setCurrent(p))
        .catch(() => setCurrent(null))
    } else {
      setCurrent(null)
    }
  }, [planetId, setCurrent, getById])

  const templateProps: HomeTemplateProps = useMemo(
    () => ({
      title: t('hello'),
      seo: current
        ? {
            title: t('planet.title', { name: current.name }),
            description: t('planet.description', { name: current.name }),
          }
        : {
            title: t('home.title'),
            description: t('home.description'),
          },
    }),
    [t, current]
  )

  return <HomeTemplate {...templateProps} planet={current} />
}

export default DashboardPage
