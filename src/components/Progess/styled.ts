import styled from 'styled-components'

export const Progress = styled.div`
  height: 200px;
  display: flex;
  align-items: center;
  justify-content: center;
`
