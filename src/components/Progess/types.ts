export type ProgressProps = {
  color?: 'primary' | 'secondary' | 'inherit'
}
