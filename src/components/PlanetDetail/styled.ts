import styled from 'styled-components'

export const Image = styled.img`
  display: block;
  width: 100%;
`

export const Content = styled.div`
  padding: 20px;
  background-color: white;
`

export const Title = styled.h1`
  ${(props) => ({ ...props.theme.typography.h1 })}
  color: ${(props) => props.theme.palette.colors.pureBlack};
  text-align: center;
`

export const Paragraph = styled.p`
  ${(props) => ({ ...props.theme.typography.body1 })}
  color: ${(props) => props.theme.palette.colors.pureBlack};
  margin: 0;
`
