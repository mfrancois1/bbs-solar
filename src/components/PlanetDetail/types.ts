import { Planet } from '../SolarProvider/types'

export type PlanetDetailProps = {
  planet: Planet
}
