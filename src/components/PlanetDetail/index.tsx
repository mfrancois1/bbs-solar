import React, { FC } from 'react'
import { Grid } from '@material-ui/core'

import { PlanetDetailProps } from './types'
import * as SC from './styled'

const PlanetDetail: FC<PlanetDetailProps> = ({ planet }) => {
  return (
    <SC.Content>
      <SC.Image src={`https://via.placeholder.com/560x300.png/000/fff?text=${planet.name}`} />
      <SC.Title>{planet.name}</SC.Title>
      <Grid container spacing={3}>
        <Grid item xs={1} sm={2} />
        <Grid item xs={11} sm={4}>
          <SC.Paragraph>Nom : {planet.name}</SC.Paragraph>
          <SC.Paragraph>Densité : {planet.density}</SC.Paragraph>
          <SC.Paragraph>Gravité : {planet.gravity}</SC.Paragraph>
        </Grid>
        <Grid item xs={11} sm={5}>
          <SC.Paragraph>Est une planète : {planet.isPlanet ? 'oui' : 'non'}</SC.Paragraph>
          <SC.Paragraph>Dimension : {planet.dimension}</SC.Paragraph>
        </Grid>
      </Grid>
    </SC.Content>
  )
}

export default PlanetDetail
