import React, { FC } from 'react'
import { Grid } from '@material-ui/core'
import { useHistory } from 'react-router-dom'

import PlanetCard from '../PlanetListItem'

import { PlanetListProps } from './types'

const PlanetList: FC<PlanetListProps> = ({ planets }) => {
  const history = useHistory()

  return (
    <Grid container spacing={3}>
      {planets.map((b, index) => (
        <Grid item xs={12} sm={4} key={index}>
          <PlanetCard onClick={(id) => history.push(`/${id}`)} planet={b} />
        </Grid>
      ))}
    </Grid>
  )
}

export default PlanetList
