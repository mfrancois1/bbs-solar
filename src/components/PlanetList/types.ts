import { Planet } from '../SolarProvider/types'

export type PlanetListProps = {
  planets: Array<Planet>
}
