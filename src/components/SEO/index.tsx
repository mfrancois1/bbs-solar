import React, { FC } from 'react'
import { Helmet } from 'react-helmet-async'

import { SEOProps } from './types'

const SEO: FC<SEOProps> = (props) => {
  const { title, description } = props

  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />
      {/* TODO : NoIndex, NoFollow, OpenGraph, Twitter */}
    </Helmet>
  )
}

export default SEO
