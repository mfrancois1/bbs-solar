import React, { FC } from 'react'
import { Card, CardActionArea, Typography } from '@material-ui/core'

import * as SC from './styled'
import { PlanetListItemProps } from './types'

const PlanetListItem: FC<PlanetListItemProps> = ({ planet, onClick }) => {
  return (
    <Card onClick={() => onClick(planet.id || '')}>
      <CardActionArea>
        <SC.Media
          image={`https://via.placeholder.com/420x150/111/fff.png?text=${planet.name}`}
          title="Contemplative Reptile"
        />
        <SC.Content>
          <Typography gutterBottom variant="h5" component="h2">
            {planet.name}
          </Typography>
          <Typography variant="body2" component="p">
            Gravity: {planet.gravity}
            <br />
            Density: {planet.density}
          </Typography>
        </SC.Content>
      </CardActionArea>
    </Card>
  )
}

export default PlanetListItem
