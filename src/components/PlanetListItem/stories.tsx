import React, { FC, PropsWithChildren, useEffect, useState } from 'react'

import defaultExport from '../../../.storybook/config/defaultExport'

import PlanetListItem from '.'
import { Story } from '@storybook/react'
import usePlanetSearch from '../UsePlanetSearch'
import Progress from '../Progess'
import styled from 'styled-components'
import SolarProvider from '../SolarProvider'
import { width } from '@material-ui/system'

const Template = (StoryComponent: Story) => (
  <SolarProvider>
    <StoryComponent />
  </SolarProvider>
)

export default defaultExport({
  title: 'Components/PlanetListItem',
  component: PlanetListItem,
  viewport: null,
  containerStyle: { padding: '3rem' },
  decorators: [Template],
  argTypes: {
    width: {
      control: {
        type: 'range',
        step: 4,
        min: 300,
        max: 500,
      },
    },
    name: {
      control: {
        type: 'text',
      },
    },
  },
})

const Container = styled.div<{ width: number }>`
  width: ${({ width }) => `${width}px`};
  margin: 0 auto;
`

export const Simple: Story<{ name: string; width: number }> = (arg) => {
  const [delay, setDelay] = useState(false)
  const { result, searching } = usePlanetSearch({
    search: arg.name,
  })

  useEffect(() => {
    if (searching) {
      setDelay(true)
      setTimeout(() => setDelay(false), 700)
    }
  }, [searching, setDelay])

  useEffect(() => {
    if (searching) {
      setDelay(true)
      setTimeout(() => setDelay(false), 700)
    }
  }, [arg.name, setDelay])

  return (
    <Container width={arg.width}>
      {searching || delay ? (
        <Progress />
      ) : result.length > 0 ? (
        <PlanetListItem planet={result[0]} onClick={() => {}} />
      ) : (
        <span>No result</span>
      )}
    </Container>
  )
}

Simple.args = {
  name: 'lune',
  width: 400,
}
