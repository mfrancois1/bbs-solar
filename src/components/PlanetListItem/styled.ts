import styled from 'styled-components'
import { CardContent, CardMedia } from '@material-ui/core'

export const Title = styled.h2`
  ${(props) => ({ ...props.theme.typography.h3 })}
  margin: 0
`

export const Paragraph = styled.p`
  margin: 0;
`

export const Media = styled(CardMedia)`
  height: 150px;
`

export const Content = styled(CardContent)`
  background: ${(props) => props.theme.palette.colors.pureWhite};
  color: ${(props) => props.theme.palette.colors.pureBlack};
  text-align: center;
`
