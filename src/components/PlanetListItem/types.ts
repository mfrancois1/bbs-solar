import { Planet } from '../SolarProvider/types'

export type PlanetListItemProps = {
  planet: Planet
  onClick: (planetId: string) => void
}
