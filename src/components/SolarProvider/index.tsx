import React, { FC, PropsWithChildren, useRef } from 'react'

import api from '../../configuration/api'
import createCtx from '../../utils/CreateCtx'

import { APIClient, Bodie } from './api'
import { SolarProviderProps } from './types'

export const [useSolarContext, Provider] = createCtx<{
  getAll: () => Promise<{
    bodies?: Array<Bodie>
  }>
  searchPlanetByName: (search: string) => Promise<{
    bodies?: Array<Bodie>
  }>
  getById: (id: string) => Promise<Bodie>
}>()

const SolarProvider: FC<PropsWithChildren<SolarProviderProps>> = ({ children }) => {
  const SolarAPI = useRef(
    new APIClient({
      baseURL: api.url || '',
    })
  )

  return (
    <Provider
      value={{
        getAll: () => SolarAPI.current.rest.bodiesService.getAll(),
        getById: (id) => SolarAPI.current.rest.bodiesService.getById(id),
        searchPlanetByName: (search) =>
          SolarAPI.current.rest.bodiesService.getAll({ filter: [`id,cs,${search}`] }),
      }}
    >
      {children}
    </Provider>
  )
}

export default SolarProvider
