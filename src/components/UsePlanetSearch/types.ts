import { Planet } from '../SolarProvider/types'

export type PlanetSearchProps = {
  search: string
}

export type PlanetSearch = {
  searching: boolean
  error: Error | null
  result: Array<Planet>
}
