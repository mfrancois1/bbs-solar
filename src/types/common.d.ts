import React from 'react'

import { UserType } from '../generated/graphql'

export type RestrictedReactFC<T> = React.FC<T> & { restrictedUserTypes?: UserType[] }

export type PageSEO = {
  title: string
  description: string
  canonical?: string
  noindex?: boolean
  nofollow?: boolean
  openGraph?: {
    url: string
    title: string
    description: string
    // TODO : Images
  }
  twitter?: {
    handle: string
    site: string
    cardType: string
    // TODO : take first og picture
  }
}

export type Page = {
  seo: PageSEO
}
